# CERN Authentication

This is a library created to abstract the process of using the
**CERN Authentication**, a *OpenID* server implemented with *Keycloak*

This documentation **does not** aim to teach the fundamentals of *OpenID*,
*OAuth* and *Keycloak*.

## Installation

Install using Composer.

```bash
composer require glance-project/cern-authentication
```

## Getting started

To use this library, you will need to have an application registered on
[Application Portal](https://application-portal.web.cern.ch). If you do not
know what this mean, refer to the [CERN Authorization Service documentation](https://auth.docs.cern.ch/applications/application-configuration)

With your application registered, you will need the **Client ID**. Depending on
the authentication flow, you will also need the **Client Secret** and a valid
**Redirect URL**.

## KeycloakProvider

`KeycloakProvider` is the main class of the library. You will need it to
perform any type of authentication.

### `KeycloakProvider::create()`

Creates a `KeycloakProvider` object.

```php
<?php

use Glance\CernAuthentication\KeycloakProvider;

$keycloakProvider = KeycloakProvider::create();
```

`KeycloakProvider::create()` optionally accepts an `$options` array, which
overwrites the default configuration:

```php
$defaultOptions = [
    "authenticationUri"    => "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect",
    "apiAccessUri"         => "https://auth.cern.ch/auth/realms/cern/api-access/token",
    "client"               => new \GuzzleHttp\Client(),
    "serverRequestFactory" => new Nyholm\Psr7\Factory\Psr17Factory(),
    "uriFactory"           => new Nyholm\Psr7\Factory\Psr17Factory(),
    "streamFactory"        => new Nyholm\Psr7\Factory\Psr17Factory(),
];
```

Only pass the values to be changed.
For example, if you want to use the QA environment from CERN Authentication:

```php
<?php

use Glance\CernAuthentication\KeycloakProvider;

$keycloakProvider = KeycloakProvider::create([
    "authenticationUri" => "https://keycloak-qa.cern.ch/auth/realms/cern/protocol/openid-connect",
    "apiAccessUri"      => "https://keycloak-qa.cern.ch/auth/realms/cern/api-access/token",
]);
```

<div class="panel panel-info">
**Note**
{: .panel-heading}
<div class="panel-body">

It is advised to use `createForProduction()` and `createForDevelopment()` instead.

</div>
</div>

### `KeycloakProvider::createForProduction()`

Similar to `KeycloakProvider::create()` but, with URIs set to production by default.

Example:

```php
<?php

use Glance\CernAuthentication\KeycloakProvider;

$inProduction = getenv("ENVIRONMENT") === "PRODUCTION"; // true or false
$keycloakProvider = $inProduction ?
    KeycloakProvider::createForProduction() :
    KeycloakProvider::createForDevelopment();
```

### `KeycloakProvider::createForDevelopment()`

Similar to `KeycloakProvider::create()` but, with URIs set to QA by default.

### `KeycloakProvider::apiAccess()`

You can use API Access to get access tokens to API which also
use the CERN Authentication.

According to the [CERN Authorization Service documentation](https://auth.docs.cern.ch/user-documentation/oidc/api-access):

> API Access is a non-standard token endpoint of the CERN Authentication server. OpenID Connect
> applications with Client Credentials Grant (a.k.a. Keycloak service accounts) can use this
> endpoint to get a valid API token for any other application, by providing a valid audience.
> The roles inside the token are the ones assigned to the Application Identity that got the
> token, defined in the target application.

To use this type of authentication, you will need an application with
**Client ID**, **Client Secret** and be able to perform the
**client credentials flow** (mark the option *My application will need to get
tokens using its own client ID and secret* on Application Portal > SSO
Registration).

The most common use of this approach is to get tokens to be used on other CERN
APIs, for example the *Authorization Service API* which includes endpoints for
dealing with groups. On the next example, we can get an access token to be used
while calling the *Authorization Service API* (Audience Client ID:
`authorization-service-api`).

```php
<?php

$clientId = getenv("MY_APP_CLIENT_ID");
$clientSecret = getenv("MY_APP_CLIENT_SECRET");
$audienceClientId = "authorization-service-api";

$provider = KeycloakProvider::create();
$result = $provider->apiAccess($clientId, $clientSecret, $audienceClientId);

echo $result->accessToken(); // eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwi...
```

### `KeycloakProvider::decodeToken()`

For token decoding, you will need the **access token** to be checked,
together with **Client ID** of the application.

```php
<?php

$clientId = getenv("MY_APP_CLIENT_ID");
$token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwi...";

$provider = KeycloakProvider::create();
$user = $provider->decodeToken($token, $clientId);

$user->personId();  // 837034
$user->username();  // mgunters
$user->email();     // mario.simao@cern.ch
$user->fullName();  // Mario Gunter Simao
$user->firstName(); // Mario
$user->lastName();  // Gunter Simao
$user->roles();     // ["admin", "test_role"]
```
