<?php

namespace Glance\CernAuthentication\Tests\Unit;

use Glance\CernAuthentication\Exception\FailedToDecodeTokenException;
use Glance\CernAuthentication\Exception\InactiveTokenException;
use Glance\CernAuthentication\Exception\ApiAccessException;
use Glance\CernAuthentication\KeycloakProvider;
use PHPUnit\Framework\TestCase;

final class KeycloakProviderTest extends TestCase
{
    public function testCreateWithDefaultOptions(): void
    {
        $provider = KeycloakProvider::create();

        $this->assertEquals(
            \GuzzleHttp\Client::class,
            get_class($provider->getClient())
        );

        $this->assertEquals(
            KeycloakProvider::PRODUCTION_AUTH_URI,
            $provider->getAuthenticationUri()
        );

        $this->assertEquals(
            KeycloakProvider::PRODUCTION_API_ACCESS_URI,
            $provider->getApiAccessUri()
        );

        $this->assertEquals(
            \Nyholm\Psr7\Factory\Psr17Factory::class,
            get_class($provider->getServerRequestFactory())
        );

        $this->assertEquals(
            \Nyholm\Psr7\Factory\Psr17Factory::class,
            get_class($provider->getUriFactory())
        );

        $this->assertEquals(
            \Nyholm\Psr7\Factory\Psr17Factory::class,
            get_class($provider->getStreamFactory())
        );
    }

    public function testApiAccess(): void
    {
        $accessToken = $this->token();

        $provider = KeycloakProvider::create([
            "client" => FakeClient::forApiAccess($accessToken)
        ]);

        $result = $provider->apiAccess(
            "sw-api-dev",
            "fd122b9d-49f3-450c-a766-adb8db0b4fb7",
            "authorization-service-api"
        );

        $this->assertEquals($result->accessToken(), $accessToken);
    }

    public function testTokenExchange(): void
    {
        $accessToken = $this->token();
        $refreshToken = $this->token();

        $provider = KeycloakProvider::create([
            "client" => FakeClient::forTokenExchange($accessToken, $refreshToken)
        ]);

        $result = $provider->tokenExchange(
            $accessToken,
            "sw-api-dev",
            "sw-dev"
        );

        $this->assertEquals($result->accessToken(), $accessToken);
    }

    public function testCreateForProduction(): void
    {
        $provider = KeycloakProvider::createForProduction();

        $this->assertEquals(KeycloakProvider::PRODUCTION_AUTH_URI, $provider->getAuthenticationUri());
        $this->assertEquals(KeycloakProvider::PRODUCTION_API_ACCESS_URI, $provider->getApiAccessUri());
    }

    public function testCreateForDevelopment(): void
    {
        $provider = KeycloakProvider::createForDevelopment();

        $this->assertEquals(KeycloakProvider::QA_AUTH_URI, $provider->getAuthenticationUri());
        $this->assertEquals(KeycloakProvider::QA_API_ACCESS_URI, $provider->getApiAccessUri());
    }

    public function testErrorOnApiAccess(): void
    {
        $provider = KeycloakProvider::create([
            "client" => FakeClient::serverError()
        ]);

        $this->expectException(ApiAccessException::class);
        $provider->apiAccess(
            "sw-api-dev",
            "supersecret",
            "authorization-service-api"
        );
    }

    private function token(): string
    {
        // phpcs:ignore Generic.Files.LineLength.TooLong
        return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTWFyaW8ifQ.0_1g4Zit9MJi0-Xc1XYaNEvm1TY9lACgQPq-DUGXMsY";
    }
}
