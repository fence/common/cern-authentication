<?php

namespace Glance\CernAuthentication\Tests\Unit\Exception;

use Glance\CernAuthentication\Exception\FailedToDecodeTokenException;
use Nyholm\Psr7\Factory\Psr17Factory;
use PHPUnit\Framework\TestCase;

final class FailedToDecodeTokenExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $factory = new Psr17Factory();
        $response = $factory->createResponse();

        $e = new FailedToDecodeTokenException($response);

        $this->assertSame("Failed decoding token.", $e->getMessage());
        $this->assertSame($response, $e->getResponse());
    }
}
