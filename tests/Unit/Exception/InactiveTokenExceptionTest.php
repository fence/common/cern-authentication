<?php

namespace Glance\CernAuthentication\Tests\Unit\Exception;

use Glance\CernAuthentication\Exception\InactiveTokenException;
use PHPUnit\Framework\TestCase;

final class InactiveTokenExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $e = new InactiveTokenException();

        $this->assertSame("Token expired.", $e->getMessage());
    }
}
