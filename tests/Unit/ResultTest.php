<?php

namespace Glance\CernAuthentication\Tests\Unit;

use Glance\CernAuthentication\Result;
use InvalidArgumentException;
use Nyholm\Psr7\Factory\Psr17Factory;
use PHPUnit\Framework\TestCase;

final class ResultTest extends TestCase
{
    public function testFromArray(): void
    {
        $accessToken = $this->accessToken();
        $array = [ "access_token" => $accessToken ];
        $result = Result::fromArray($array);

        $this->assertEquals($accessToken, $result->accessToken());
    }

    public function testFromArrayWithoutAccessToken(): void
    {
        $array = [ "expires_in" => 3000 ];

        $this->expectException(InvalidArgumentException::class);
        $result = Result::fromArray($array);
    }

    public function testFromApiAccessResponse(): void
    {
        $accessToken = $this->accessToken();

        $factory = new Psr17Factory();
        $body = $factory->createStream(
            json_encode([
                "access_token"       => $accessToken,
                "expires_in"         => 1199,
                "refresh_expires_in" => 0,
                "token_type"         => "bearer",
                "not-before-policy"  => 0,
                "session_state"      => "4ad1acc2-c305-45af-9b59-cb604cd51414",
                "scope"              => "profile email identity-provider",
            ])
        );

        $response = $factory->createResponse()->withBody($body);

        $result = Result::fromResponse($response);

        $this->assertEquals($accessToken, $result->accessToken());
    }

    public function testFromTokenExchangeResponse(): void
    {
        $accessToken = $this->accessToken();

        $factory = new Psr17Factory();
        $body = $factory->createStream(
            json_encode([
                "access_token"       => $accessToken,
                "expires_in"         => 1199,
                "refresh_expires_in" => 36000,
                "refresh_token"      => $this->accessToken(),
                "token_type"         => "bearer",
                "not-before-policy"  => 0,
                "session_state"      => "4ad1acc2-c305-45af-9b59-cb604cd51414",
                "scope"              => "profile email identity-provider",
            ])
        );

        $response = $factory->createResponse()
                            ->withBody($body);

        $result = Result::fromResponse($response);

        $this->assertEquals($accessToken, $result->accessToken());
    }

    private function accessToken(): string
    {
        // phpcs:ignore Generic.Files.LineLength.TooLong
        return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTWFyaW8ifQ.0_1g4Zit9MJi0-Xc1XYaNEvm1TY9lACgQPq-DUGXMsY";
    }
}
