<?php

namespace Glance\CernAuthentication\Tests\Unit;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final class FakeClient implements ClientInterface
{
    private $fakeResponse;

    private function __construct(ResponseInterface $fakeResponse)
    {
        $this->fakeResponse = $fakeResponse;
    }

    public static function forApiAccess(string $accessToken): self
    {
        $factory = new \Nyholm\Psr7\Factory\Psr17Factory();
        $body = $factory->createStream(
            json_encode([
                "access_token"       => $accessToken,
                "expires_in"         => 1199,
                "refresh_expires_in" => 0,
                "token_type"         => "bearer",
                "not-before-policy"  => 0,
                "session_state"      => "4ad1acc2-c305-45af-9b59-cb604cd51414",
                "scope"              => "profile email identity-provider",
            ])
        );

        $fakeResponse = $factory->createResponse()->withBody($body);

        return new self($fakeResponse);
    }

    public static function forTokenExchange(string $accessToken, string $refreshToken): self
    {
        $factory = new \Nyholm\Psr7\Factory\Psr17Factory();
        $body = $factory->createStream(
            json_encode([
                "access_token"       => $accessToken,
                "expires_in"         => 1199,
                "refresh_expires_in" => 0,
                "refresh_token"      => $refreshToken,
                "token_type"         => "bearer",
                "not-before-policy"  => 36000,
                "session_state"      => "4ad1acc2-c305-45af-9b59-cb604cd51414",
                "scope"              => "profile email",
            ])
        );

        $fakeResponse = $factory->createResponse()->withBody($body);

        return new self($fakeResponse);
    }

    public static function clientError(): self
    {
        $factory = new \Nyholm\Psr7\Factory\Psr17Factory();
        $fakeResponse = $factory->createResponse(400);
        $fakeResponse->getBody()->write(json_encode([
            "error" => "error_code",
            "error_description" => "Error message."
        ]));

        return new self($fakeResponse);
    }

    public static function serverError(): self
    {
        $factory = new \Nyholm\Psr7\Factory\Psr17Factory();
        $fakeResponse = $factory->createResponse(500);
        $fakeResponse->getBody()->write(json_encode([
            "error" => "error_code",
            "error_description" => "Error message."
        ]));

        return new self($fakeResponse);
    }

    public static function emptyBody(): self
    {
        $factory = new \Nyholm\Psr7\Factory\Psr17Factory();
        $fakeResponse = $factory->createResponse();

        return new self($fakeResponse);
    }

    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        return $this->fakeResponse;
    }
}
