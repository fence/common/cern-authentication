<?php

namespace Glance\CernAuthentication\Tests\Unit;

use Glance\CernAuthentication\Exception\FailedToDecodeTokenException;
use Glance\CernAuthentication\Exception\InactiveTokenException;
use Glance\CernAuthentication\User;
use Nyholm\Psr7\Factory\Psr17Factory;
use PHPUnit\Framework\TestCase;

final class UserTest extends TestCase
{
    public function testFromArray(): void
    {
        $array = $this->validData();

        $user = User::fromArray($array);

        $this->assertSame($array["cern_person_id"], $user->personId());
        $this->assertSame($array["preferred_username"], $user->username());
        $this->assertSame($array["email"], $user->email());
        $this->assertSame($array["given_name"], $user->firstName());
        $this->assertSame($array["family_name"], $user->lastName());
        $this->assertSame($array["name"], $user->fullName());
        $this->assertSame($array["cern_roles"], $user->roles());
    }

    public function testFromResponse(): void
    {
        $data = $this->validData();

        $factory = new Psr17Factory();
        $body = $factory->createStream(json_encode($data));
        $response = $factory->createResponse()->withBody($body);

        $user = User::fromResponse($response);

        $this->assertSame($data["cern_person_id"], $user->personId());
        $this->assertSame($data["preferred_username"], $user->username());
        $this->assertSame($data["email"], $user->email());
        $this->assertSame($data["given_name"], $user->firstName());
        $this->assertSame($data["family_name"], $user->lastName());
        $this->assertSame($data["name"], $user->fullName());
        $this->assertSame($data["cern_roles"], $user->roles());
    }

    private function validData(): array
    {
        return [
            "active"         => true,
            "cern_person_id" => 837034,
            "preferred_username" => "mgunters",
            "email"          => "mario.simao@cern.ch",
            "given_name"     => "Mario",
            "family_name"    => "Gunter Simao",
            "name"           => "Mario Gunter Simao",
            "cern_roles"     => ["admin", "test_role"],
        ];
    }
}
