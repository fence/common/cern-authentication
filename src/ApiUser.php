<?php

namespace Glance\CernAuthentication;

/**
 * User obtained with token decoding
 *
 * @psalm-type ResponseArray = array{
 *     active: bool,
 *     clientId: string,
 *     cern_roles: array<string>
 * }
 */
class ApiUser
{
    /** @var string */
    private $clientId;

    /** @var string[] */
    private $roles;

    /** @param string[] $roles */
    private function __construct(
        string $clientId,
        array $roles
    ) {
        $this->clientId = $clientId;
        $this->roles = $roles;
    }

    /** @psalm-param ResponseArray $array */
    public static function fromArray(array $array): self
    {
        return new self(
            $array["clientId"],
            $array["cern_roles"]
        );
    }

    public function clientId(): string
    {
        return $this->clientId;
    }

    /** @return string[] */
    public function roles(): array
    {
        return $this->roles;
    }
}
