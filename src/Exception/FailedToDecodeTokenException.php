<?php

namespace Glance\CernAuthentication\Exception;

use Exception;
use Psr\Http\Message\ResponseInterface;

class FailedToDecodeTokenException extends Exception
{
    /** @var ResponseInterface */
    private $response;

    public function __construct(ResponseInterface $response)
    {
        parent::__construct("Failed decoding token.");

        $this->response = $response;
    }

    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }
}
