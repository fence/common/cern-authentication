<?php

namespace Glance\CernAuthentication\Exception;

use Exception;

class InactiveTokenException extends Exception
{
    public function __construct()
    {
        parent::__construct("Token expired.");
    }
}
