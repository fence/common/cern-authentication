<?php

namespace Glance\CernAuthentication\Exception;

use Exception;
use Psr\Http\Message\ResponseInterface;

class ApiAccessException extends Exception
{
    public function __construct(ResponseInterface $response)
    {
        /** @var array{
         *      error: string,
         *      error_description: string
         * } */
        $body = json_decode((string) $response->getBody(), true);
        $message = $body["error_description"];

        parent::__construct($message);
    }
}
