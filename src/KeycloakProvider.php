<?php

namespace Glance\CernAuthentication;

use Glance\CernAuthentication\Exception\FailedToDecodeTokenException;
use Glance\CernAuthentication\Exception\InactiveTokenException;
use Glance\CernAuthentication\Exception\ApiAccessException;
use GuzzleHttp\Client;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Firebase\JWT\JWT;
use Firebase\JWT\JWK;

/**
 * Keycloak Provider
 *
 * @author Mario Simao <mario.simao@cern.ch>
 *
 * @psalm-type OptionsArray = array{
 *     authenticationUri?: string,
 *     apiAccessUri?: string,
 *     client?: ClientInterface,
 *     serverRequestFactory?: ServerRequestFactoryInterface,
 *     uriFactory?: UriFactoryInterface,
 *     streamFactory?: StreamFactoryInterface
 * }
 */
class KeycloakProvider
{
    public const PRODUCTION_AUTH_URI = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect";
    public const PRODUCTION_API_ACCESS_URI = "https://auth.cern.ch/auth/realms/cern/api-access/token";

    public const QA_AUTH_URI = "https://keycloak-qa.cern.ch/auth/realms/cern/protocol/openid-connect";
    public const QA_API_ACCESS_URI = "https://keycloak-qa.cern.ch/auth/realms/cern/api-access/token";


    /** @var ClientInterface */
    private $client;

    /** @var string */
    private $authenticationUri;

    /** @var string */
    private $apiAccessUri;

    /** @var ServerRequestFactoryInterface */
    private $serverRequestFactory;

    /** @var UriFactoryInterface */
    private $uriFactory;

    /** @var StreamFactoryInterface */
    private $streamFactory;

    private function __construct(
        ClientInterface $client,
        string $authenticationUri,
        string $apiAccessUri,
        ServerRequestFactoryInterface $serverRequestFactory,
        UriFactoryInterface $uriFactory,
        StreamFactoryInterface $streamFactory
    ) {
        $this->client = $client;
        $this->authenticationUri = $authenticationUri;
        $this->apiAccessUri = $apiAccessUri;
        $this->serverRequestFactory = $serverRequestFactory;
        $this->uriFactory = $uriFactory;
        $this->streamFactory = $streamFactory;
    }

    /**
     * @psalm-param OptionsArray $options
     */
    public static function create(array $options = []): self
    {
        $defaultOptions = [
            "authenticationUri"    => self::PRODUCTION_AUTH_URI,
            "apiAccessUri"         => self::PRODUCTION_API_ACCESS_URI,
            "client"               => null,
            "serverRequestFactory" => null,
            "uriFactory"           => null,
            "streamFactory"        => null,
        ];

        $options = array_merge($defaultOptions, $options);

        $client = $options["client"];
        if (!$client) {
            $client = new Client();
        }

        $serverRequestFactory = $options["serverRequestFactory"];
        if (!$serverRequestFactory) {
            $serverRequestFactory = new Psr17Factory();
        }

        $uriFactory = $options["uriFactory"];
        if (!$uriFactory) {
            $uriFactory = new Psr17Factory();
        }

        $streamFactory = $options["streamFactory"];
        if (!$streamFactory) {
            $streamFactory = new Psr17Factory();
        }

        $authenticationUri = $options["authenticationUri"];
        $apiAccessUri = $options["apiAccessUri"];

        return new self(
            $client,
            $authenticationUri,
            $apiAccessUri,
            $serverRequestFactory,
            $uriFactory,
            $streamFactory
        );
    }

    /**
     * @psalm-param OptionsArray $options
     */
    public static function createForProduction(array $options = []): self
    {
        $defaultOptions = [
            "authenticationUri" => self::PRODUCTION_AUTH_URI,
            "apiAccessUri"      => self::PRODUCTION_API_ACCESS_URI,
        ];

        $options = array_merge($defaultOptions, $options);

        return self::create($options);
    }

    /**
     * @psalm-param OptionsArray $options
     */
    public static function createForDevelopment(array $options = []): self
    {
        $defaultOptions = [
            "authenticationUri" => self::QA_AUTH_URI,
            "apiAccessUri"      => self::QA_API_ACCESS_URI,
        ];

        $options = array_merge($defaultOptions, $options);

        return self::create($options);
    }

    /**
     * Get access token to be used on an external API.
     *
     * Login as application
     *
     * The external API must use CERN Keycloak for authentication.
     *
     * This method uses a non-standard token endpoint of the CERN Authentication
     * server, called API Access. More information can be found on CERN
     * Authorization Service documentation.
     *
     * @see https://auth.docs.cern.ch/user-documentation/oidc/api-access
     *      API Access documentation
     */
    public function apiAccess(
        string $clientId,
        string $clientSecret,
        string $audienceClientId
    ): Result {
        $uri = $this->uriFactory->createUri($this->apiAccessUri)
                                ->withUserInfo($clientId, $clientSecret);

        $body = $this->streamFactory->createStream(
            \http_build_query([
                "grant_type" => "client_credentials",
                "audience" => $audienceClientId,
            ])
        );

        $factory = $this->serverRequestFactory;
        $request = $factory->createServerRequest("POST", $uri)
                           ->withAddedHeader("Content-Type", "application/x-www-form-urlencoded")
                           ->withBody($body);

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            throw new ApiAccessException($response);
        }

        return Result::fromResponse($response);
    }

    public function tokenExchange(
        string $subjectToken,
        string $audienceClientId,
        string $clientId,
        ?string $clientSecret = null
    ): Result {
        $uri = $this->uriFactory->createUri($this->authenticationUri . "/token")
                                ->withUserInfo($clientId, $clientSecret);

        $body = $this->streamFactory->createStream(
            \http_build_query([
                "subject_token"        => $subjectToken,
                "audience"             => $audienceClientId,
                "grant_type"           => "urn:ietf:params:oauth:grant-type:token-exchange",
                "requested_token_type" => "urn:ietf:params:oauth:token-type:refresh_token",
            ])
        );

        $factory = $this->serverRequestFactory;
        $request = $factory->createServerRequest("POST", $uri)
                           ->withAddedHeader("Content-Type", "application/x-www-form-urlencoded")
                           ->withBody($body);

        $response = $this->client->sendRequest($request);

        return Result::fromResponse($response);
    }

    /**
     * Create user on given application from given token
     *
     * @param string $token The token to decode
     * @param string $clientId The client ID of the application
     *
     * @return User|ApiUser The user or API user
     *
     * @throws FailedToDecodeTokenException Could not  the token
     */
    public function createUser(
        string $token,
        string $clientId
    ) {
        $decodedToken = $this->decodeToken(
            $token,
            $clientId
        );

        /**
        * @psalm-suppress ArgumentTypeCoercion
        * @psalm-suppress MixedArgumentTypeCoercion
        */
        return isset($decodedToken["cern_person_id"])
            ? User::fromArray($decodedToken)
            : ApiUser::fromArray($decodedToken);
    }

    /**
     * Verifies the signature of the token and decodes it.
     *
     * @param string $encodedToken The encoded JWT (Json Web Token)
     * @param string $clientId The client ID
     *
     * @return array $decodedToken Data from the decoded token.
     *
     * @throws FailedToDecodeTokenException  Could not decode the token.
     */
    public function decodeToken(
        $encodedToken,
        $clientId
    ): array {
        $httpClient = new Client();

        $response = $httpClient->get(
            "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/certs"
        );

        if ($response->getStatusCode() >= 400) {
            throw new FailedToDecodeTokenException($response);
        }

        /** @var array|null $body */
        $body = json_decode((string) $response->getBody(), true);

        if ($body === null) {
            throw new FailedToDecodeTokenException($response);
        }

        $jwks = ["keys" => $body["keys"]];

        try {
            $decodedToken = JWT::decode($encodedToken, JWK::parseKeySet($jwks));
        } catch (\Exception $e) {
            throw new InactiveTokenException();
        }

        if ($decodedToken->aud !== $clientId) {
            throw new FailedToDecodeTokenException($response);
        }

        return (array) $decodedToken;
    }

    /**
     * Verifies the signature of the token and decodes it.
     * This method is used when the introspection endpoint returns that the
     * token is not active, which could be wrong as it may be a token sent by an API,
     * in which case the introspect endpoint will not work properly.
     * This method is used as a fallback. However, if the token is indeed expired,
     * the PHP JWT library will throw an exception.
     *
     * @param string $encodedToken The encoded JWT (Json Web Token)
     *
     * @return array $decodedToken Data from the decoded token.
     *
     * @throws FailedToDecodeTokenException  Could not decode the token.
     */
    private function decodeInactiveToken($encodedToken): array
    {
        $httpClient = new Client();

        $clientId = getenv("CLIENT_ID");

        $response = $httpClient->get(
            "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/certs"
        );

        if ($response->getStatusCode() >= 400) {
            throw new FailedToDecodeTokenException($response);
        }

        /** @var array|null $body */
        $body = json_decode((string) $response->getBody(), true);

        if ($body === null) {
            throw new FailedToDecodeTokenException($response);
        }

        $jwks = ["keys" => $body["keys"]];

        try {
            $decodedToken = JWT::decode($encodedToken, JWK::parseKeySet($jwks));
        } catch (\Exception $e) {
            throw new InactiveTokenException();
        }

        if ($decodedToken->aud !== $clientId) {
            throw new FailedToDecodeTokenException($response);
        }

        return (array) $decodedToken;
    }

    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function getAuthenticationUri(): string
    {
        return $this->authenticationUri;
    }

    public function getApiAccessUri(): string
    {
        return $this->apiAccessUri;
    }

    public function getServerRequestFactory(): ServerRequestFactoryInterface
    {
        return $this->serverRequestFactory;
    }

    public function getUriFactory(): UriFactoryInterface
    {
        return $this->uriFactory;
    }

    public function getStreamFactory(): StreamFactoryInterface
    {
        return $this->streamFactory;
    }
}
