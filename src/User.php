<?php

namespace Glance\CernAuthentication;

use Psr\Http\Message\ResponseInterface;

/**
 * User obtained with token decoding
 *
 * @author Mario Simao <mario.simao@cern.ch>
 *
 * @psalm-type ResponseArray = array{
 *     active: bool,
 *     cern_person_id: int,
 *     preferred_username: string,
 *     email: string,
 *     given_name: string,
 *     family_name: string,
 *     name: string,
 *     cern_roles: array<string>
 * }
 */
class User
{
    /** @var int */
    private $personId;

    /** @var string */
    private $username;

    /** @var string */
    private $email;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $fullName;

    /** @var string[] */
    private $roles;

    /** @param string[] $roles */
    private function __construct(
        int $personId,
        string $username,
        string $email,
        string $firstName,
        string $lastName,
        string $fullName,
        array $roles
    ) {
        $this->personId = $personId;
        $this->username = $username;
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->fullName = $fullName;
        $this->roles = $roles;
    }

    /** @psalm-param ResponseArray $array */
    public static function fromArray(array $array): self
    {
        $personId = $array["cern_person_id"];
        $username = $array["preferred_username"];
        $email = $array["email"];
        $firstName = $array["given_name"];
        $lastName = $array["family_name"];
        $fullName = $array["name"];
        $roles = $array["cern_roles"];

        return new self(
            $personId,
            $username,
            $email,
            $firstName,
            $lastName,
            $fullName,
            $roles
        );
    }

    public static function fromResponse(ResponseInterface $response): self
    {
        /** @psalm-var ResponseArray $decodedToken */
        $decodedToken = json_decode((string) $response->getBody(), true);

        return self::fromArray($decodedToken);
    }

    public function personId(): int
    {
        return $this->personId;
    }

    public function username(): string
    {
        return $this->username;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function firstName(): string
    {
        return $this->firstName;
    }

    public function lastName(): string
    {
        return $this->lastName;
    }

    public function fullName(): string
    {
        return $this->fullName;
    }

    /** @return string[] */
    public function roles(): array
    {
        return $this->roles;
    }
}
