<?php

namespace Glance\CernAuthentication;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;

/**
 * Result obtained with api-access or token exchange
 *
 * @author Mario Simao <mario.simao@cern.ch>
 *
 * @psalm-type ResultArray = array{
 *     access_token: string,
 *     expires_in?: int,
 *     refresh_token?: string,
 *     refresh_expires_in?: int,
 *     token_type?: string,
 *     not-before-policy?: int,
 *     session_state?: string,
 *     scope?: string
 * }
 */
class Result
{
    /** @var string */
    private $accessToken;

    /** @var int|null */
    private $accessTokenExpirationInSeconds;

    /** @var string|null */
    private $refreshToken;

    /** @var int|null */
    private $refreshTokenExpirationInSeconds;

    /** @var string|null */
    private $tokenType;

    /** @var int|null */
    private $notBeforePolicy;

    /** @var string|null */
    private $sessionState;

    /** @var array */
    private $scopes;

    private function __construct(
        string $accessToken,
        ?int $accessTokenExpirationInSeconds,
        ?string $refreshToken,
        ?int $refreshTokenExpirationInSeconds,
        ?string $tokenType,
        ?int $notBeforePolicy,
        ?string $sessionState,
        array $scopes
    ) {
        $this->accessToken = $accessToken;
        $this->accessTokenExpirationInSeconds = $accessTokenExpirationInSeconds;
        $this->refreshToken = $refreshToken;
        $this->refreshTokenExpirationInSeconds = $refreshTokenExpirationInSeconds;
        $this->tokenType = $tokenType;
        $this->notBeforePolicy = $notBeforePolicy;
        $this->sessionState = $sessionState;
        $this->scopes = $scopes;
    }

    /** @psalm-param ResultArray $array */
    public static function fromArray(array $array): self
    {
        if (empty($array['access_token'])) {
            throw new InvalidArgumentException('Required option not passed: "access_token"');
        }

        $accessToken = $array["access_token"];
        $accessTokenExpirationInSeconds = $array["expires_in"] ?? null;
        $refreshToken = $array["refresh_token"] ?? null;
        $refreshTokenExpirationInSeconds = $array["refresh_expires_in"] ?? null;
        $tokenType = $array["token_type"] ?? null;
        $notBeforePolicy = $array["not-before-policy"] ?? null;
        $sessionState = $array["session_state"] ?? null;
        $scopes = isset($array["scope"]) ? explode(" ", $array["scope"]) : [];

        return new self(
            $accessToken,
            $accessTokenExpirationInSeconds,
            $refreshToken,
            $refreshTokenExpirationInSeconds,
            $tokenType,
            $notBeforePolicy,
            $sessionState,
            $scopes
        );
    }

    public static function fromResponse(ResponseInterface $response): self
    {
        /** @psalm-var ResultArray $body */
        $body = json_decode((string) $response->getBody(), true);

        return self::fromArray($body);
    }

    public function accessToken(): string
    {
        return $this->accessToken;
    }
}
